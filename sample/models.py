from django.db import models

# Create your models here.



class TestObj(models.Model):
	def get_pairs_count(self):
		count = self.pair_set.count()
		return count


		


class Pair(models.Model):
	int_A = models.IntegerField(default=0)
	int_B = models.IntegerField(default=0)
	test = models.ForeignKey(TestObj, on_delete=models.CASCADE)


class Result(models.Model):
	test = models.ForeignKey(TestObj, on_delete=models.CASCADE)
	traceback = models.TextField(null=True)
	result = models.TextField(null=True)
	date = models.DateTimeField(auto_now=True)
	round = models.IntegerField(default=0)


def get_round():
	res = Result.objects.values_list('round').order_by('-id')[0][0]
	return res