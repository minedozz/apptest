from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^add', views.add_sample, name='add_sample'),
    url(r'^test/all', views.TestsView.as_view(), name='all_tests'),
    url(r'^start/', views.start, name='start'),
    url(r'^exception/', views.ExceptionsView.as_view(), name='exception'),
    url(r'^(?P<test_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^elements/', views.elements, name='elements'),
    url(r'^forms/', views.forms, name='forms'),
    url(r'^pool/', views.pool, name='pool'),

]