from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, render,get_object_or_404
from django.template import RequestContext, loader
from django.core.urlresolvers import reverse
from django.views import generic
from celery import chain
import json
import urllib3

from .tasks import getting,func,result
from .models import TestObj, Pair, Result, get_round

def index(request):
	latest_tests_list = TestObj.objects.order_by('-id')[:10]

	res = Result.objects.filter(round=get_round())
	res = res.exclude(result=None)

	if len(res)==0: ex = 1
	else: ex =0
	context = {'latest_tests_list': latest_tests_list,'ex':ex
			}
	return render(request, 'sample/index.html', context)




def add_sample(request, *args, **kwargs):
	d = '['+request.POST['sample_text']+']'
	pairs = json.loads(d)
	test = TestObj.objects.create()
	test.save()
	for pair in pairs:
		a = Pair.objects.create(int_A=pair['a'], int_B=pair['b'], test=test)
		a.save()

	return HttpResponseRedirect(reverse('sample:index'))


class TestsView(generic.ListView):
    template_name = 'sample/all_tests.html'
    context_object_name = 'tests_list'

    def get_queryset(self):
        return TestObj.objects.all()


def start(request, *args, **kwargs):
	tests = TestObj.objects.values_list('id')
	round = get_round()

	for test in tests:
		chain(getting.s(test[0]), func.s(), result.s([test[0],round]))()

	return HttpResponseRedirect(reverse('sample:index'))

def detail(request, test_id):
    test = TestObj.objects.get(id=test_id)
    result = Result.objects.filter(round=get_round(),test_id=test_id)[0]

    return render(request, 'sample/detail.html', {'test': test,'result':result})

class ExceptionsView(generic.ListView):
    template_name = 'sample/all_tests.html'
    context_object_name = 'tests_list'

    def get_queryset(self):
        return TestObj.objects.filter(result__result=None,result__round=get_round())

def elements(request, *args, **kwargs):
    return render(request, 'sample/bootstrap-elements.html')

def forms(request, *args, **kwargs):
	return render(request, 'sample/forms.html')


def pool(request, *args, **kwargs):
	http = urllib3.PoolManager()
	act=request.POST['act']
	size=request.POST['size']
	worker=request.POST['worker']

	r = http.request('POST', 'localhost:5555/api/worker/pool/'+act+'/celery@'+worker+'?n='+size)
	return HttpResponseRedirect(reverse('sample:index'))