from __future__ import absolute_import, unicode_literals
from celery import shared_task, task

from .models import Pair, Result, TestObj, get_round
import json

@task(queue="red", routing_key="red")
def getting(id):
	pairs = Pair.objects.filter(test=id)
	result = []
	for pair in pairs:
		a = {
			'a':pair.int_A,
			'b':pair.int_B
			}
		result.append(a)

	return json.dumps(result)

@task(queue="green", routing_key="green")
def func(pairs):
	try:
		pairs = json.loads(pairs)
		result = 0
		for pair in pairs:
			result += pair['a']-pair['b']

		return json.dumps({'result': result})
	except Exception as e:
		return json.dumps({'error': str(e)})


@task(queue="blue", routing_key="blue")
def result(result, id):
	result = json.loads(result)
#	a = TestObj.objects.get(id=id)
	round = id[1]
	try: 
		res = Result(test_id=id[0],traceback=result['error'],round=round+1)
	except Exception:
		res = Result(test_id=id[0],result=result['result'],round=round+1)

	res.save()

	return round

