from django.contrib import admin
from .models import Pair, TestObj, Result



class PairInline(admin.StackedInline):
    model = Pair

    extra = 0


class TestObjAdmin(admin.ModelAdmin):
    fields = ('status',)

    inlines = [PairInline]

admin.site.register(TestObj, TestObjAdmin)